# Домашние задания Scala. AB Весна 2023

[Список тем для курсового проекта](projects.md)

- [задание №1](homework_1.md)
- [задание №2](homework_2.md)
- [задание №3](homework_3.md)
- [задание №4](homework_4.md)
- [задание №5](homework_5.md)
- [задание №6](homework_6.md)
- [задание №7](homework_7.md)
- [задание №8](homework_8.md)
- [задание №9](homework_9.md)
- [задание №10](homework_10.md)
