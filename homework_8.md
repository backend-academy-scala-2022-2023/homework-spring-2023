# Домашнее задание

Срок сдачи этого дз - **2 недели**

Начальное состояние ветки master: **замёржена как минимум HW6** (дз с накатываем миграций в бд).

В этом дз будем добалвять RBAC в ваш проект. В имеющийся сервис нужно добавить пользователей, роли пользователей и авторизацию.

Для этого:

1) Определить возможные роли пользователей в вашем сервисе. Например, Owner/Admin/User.
2) Создать таблицу для хранения информации о пользователях. В нем должны храниться:
   - id пользователя
   - роль пользователя
   - пароль пользователя. Перед этим стоит изучить вопрос, как можно безопасно хранить пароли в базе (очень сложные исхищрения применять не надо, достаточно простой защиты)
3) Добавить CRUD-методы по созданию/получению/обновлению/удалению пользователей
4) На CRUD-методы пользователей применить Rbac-модель (например, только Admin может добалять и удалять пользователей)
5) Добавить Basic Auth в ваш сервис
6) Покрыть изменения тестами

## Оформление

Дз должно быть оформлено в качестве мерж-реквеста. МР должен быть оформлен в том же формате, как и предыдущие дз. В CI
должны запущены пайплайны (если используете локальный раннер гиталаба) и проходить все стейджы.

# Как и за что получить баллы?

- Добавлены dao-слой для создания пользоватей = 2 балла;
   - Новые методы дао покрыты тестами. Для теста дао были использованы миграции с добавлением новой таблицы = 0,5 баллов;
- Добавлены CRUD-методы для пользователей = 1 балла;
   - Новые CRUD-методы дао покрыты тестами  = 0,5 балл;
- Применена Rbac-модель = 2 балл;
   - Rbac покрыт тестами = 0,5 баллов;
- Добавлена базовая аутентификация (Basic Auth) = 2 балла;
   - Покрытие Basic Auth тестами = 0,5 балл;
- Выполнено ревью сокурсника = 1 балл.



# За что можно потерять баллы?

- В МР не запущены пайплайны (или пайплайнов в принципе нет) = -5 баллов;
- В пайплайнах МР есть проваленные стейджи = -2 балла (за каждый проваленный стейдж)
- Несоблюдение CodeStyle (см. раздел CodeStyle)

# Дедлайны:

Дз должно быть сдано на проверку спустя 2 недели после выдачи задания.

Если дз было сдано **позднее 2 недель** - к баллам применяется коэффициент **0.75** с математ. округлением

Если дз было сдано **позднее 3 недель** - применится коэффициент **0.5**

# CodeStyle:

* За использование mutable коллекций = -1 балл (за каждое использование)
* За использование var переменных = -1 балл (за каждое объявление)
* Логика не отделена от описания эндпойнтов = -1 балл (если логика совмещена с описанием)
* Использование эффектов (например `Random`) = предупреждение (если эффекты не используются)
* Наличие `warn` в CI = предупреждение
* Не нужный `unsafeRunSync` = предупреждение